# Start tmux
[[ $- == *i* && -z "$TMUX" && -z "$EMACS" && $TERM == "xterm-256color" && $TERM_PROGRAM != "vscode" ]] && exec tmux


# env vars
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CONFIG_HOME=$HOME/.config
export EDITOR=nvim
export GOPATH=$XDG_DATA_HOME/go

# path
export PATH=$PATH:$HOME/.local/bin
export PATH=$PATH:$GOPATH/bin
export PATH=$PATH:$XDG_DATA_HOME/gem/ruby/3.0.0/bin
export PATH=$PATH:$XDG_DATA_HOME/go/bin

# virtualenvwrapper
export VIRTUALENVWRAPPER_PYTHON=/usr/bin/python3
export PROJECT_HOME=$HOME/src
export WORKON_HOME=$HOME/.local/share/venvs
export VIRTUALENVWRAPPER_VIRTUALENV=/usr/bin/virtualenv
source /usr/share/virtualenvwrapper/virtualenvwrapper_lazy.sh

# oh-my-zsh
export ZSH="$XDG_DATA_HOME/oh-my-zsh"
export ZSH_CUSTOM="$ZSH/custom"
ZSH_THEME="robbyrussell"
plugins=( 
    zsh-autosuggestions
    git
)
source $ZSH/oh-my-zsh.sh

source $XDG_CONFIG_HOME/zsh/zshrc-private

# hist
HISTFILE="$HOME/.history"
HISTSIZE=50000
SAVEHIST=50000
setopt HIST_IGNORE_ALL_DUPS


# aliases
if [[ $- == *i* && -z "$EMACS" ]]; then
	alias vi="nvim"
	alias vim="nvim"
elif [[ $- == *i* ]]; then
	alias vi="micro"
	alias vim="micro"
fi
alias gs='git status '
alias ga='git add '
alias gb='git branch '
alias gc='git commit'
alias gd='git diff'
alias gco='git checkout '
alias gk='gitk --all&'
alias gx='gitx --all'
alias got='git '
alias get='git '

alias upd='sudo apt update && \
	sudo apt list --upgradable && \
	sudo apt upgrade'

# key-binds
bindkey '^K' history-search-backward
bindkey '^J' history-search-forward
bindkey '^H' backward-word
bindkey '^L' forward-word
bindkey '^P' clear-screen
bindkey '^U' accept-line

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=#767676'
