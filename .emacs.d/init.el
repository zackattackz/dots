;;; -*- lexical-binding: t -*-

;; straight
(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 6))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/radian-software/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))


(setq straight-vc-git-default-clone-depth 1)
(setq straight-disable-native-compile t)


;; other requires
(straight-use-package 'hydra)
(require 'view)


;; custom functions
(defun up-directory (arg)
  "Move up a directory (delete backwards to /)."
  (interactive "p")
  (if (string-match-p "/." (minibuffer-contents))
      (zap-up-to-char (- arg) ?/)
    (delete-minibuffer-contents)))

(defun edit-init-file ()
  (interactive)
  (find-file (expand-file-name (file-name-concat user-emacs-directory "init.el"))))


(defhydra zmacs-launch (:color blue
                               :hint nil
                               :exit t)
  "
_d_: dired in project
_f_: find file in project
_e_: mu4e
_m_: emms
_p_: find project
_r_: dired
_t_: find file
"
  ("d" project-dired)
  ("f" project-find-file)
  ("e" mu4e)
  ("m" emms)
  ("p" project-switch-project)
  ("r" dired)
  ("t" find-file))

(defun split-window-below-follow ()
  (interactive)
  (split-window-below)
  (balance-windows)
  (other-window 1)
  (zmacs-launch/body))

(defun split-window-right-follow ()
  (interactive)
  (split-window-right)
  (balance-windows)
  (other-window 1)
  (zmacs-launch/body))

(defun zmacs-edit-init ()
  (interactive)
  (find-file (concat user-emacs-directory "init.el")))

(defun last-to-first (list)
  (cons (car (last list)) (butlast list)))

(defun rotate-mark-ring (arg)
  (message (format "%s" arg))
  (when (> (length mark-ring) mark-ring-max)
    (setq mark-ring (last-to-first mark-ring))))

(defun zmacs-edit-binds ()
  (interactive)
  (find-file (expand-file-name "bindings.org" user-emacs-directory)))


;; tangle
(defun zmacs-load-config (config-name &optional force-tangle)
  "Load configuration by CONFIG-NAME.
Firstly try load the CONFIG-NAME.el file.
If the elisp file was not found, tangle the CONFIG-NAME.org to generate one.

If FORCE-TANGLE is non-nil, always tangle before load . "
  (let* ((source (expand-file-name (format "%s.org" config-name) user-emacs-directory))
	 (tangle-dir (expand-file-name "tangle" user-emacs-directory))
	 (target (expand-file-name (format "%s.el" config-name) tangle-dir)))
    (when (file-exists-p source)
      (make-directory tangle-dir t)
      (when (or force-tangle (not (file-exists-p target)))
	(require 'org)
	(require 'ob)
	(org-babel-tangle-file source target))
      (load-file target))))


(zmacs-load-config "bindings" t)


;; gcmh
(straight-use-package 'gcmh)
(require 'gcmh)
(gcmh-mode 1)


;; defaults
(scroll-bar-mode -1)
(tool-bar-mode -1)
(menu-bar-mode -1)
(set-fringe-mode 10)

(savehist-mode 1)

(add-hook 'after-make-frame-functions
          (lambda (frame)
            (set-window-scroll-bars (minibuffer-window frame) 0 nil 0 nil t)))

(add-hook 'prog-mode-hook #'display-line-numbers-mode)
(add-hook 'org-mode-hook #'display-line-numbers-mode)

(window-divider-mode 1)

(show-paren-mode 1)

(global-subword-mode 1)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(recentf-mode 1)

(advice-add 'set-mark-command :before 'rotate-mark-ring)

(setq-default
  inhibit-startup-screen t
  inhibit-startup-message t
  ; Don't display comp warnings
  warning-suppress-log-types '((comp))
  ; Don't create lockfiles
  create-lockfiles nil
  ; Prefer UTF8
  buffer-file-coding-system 'utf-8-unix
  default-file-name-coding-system 'utf-8-unix
  default-keyboard-coding-system 'utf-8-unix
  default-process-coding-system '(utf-8-unix . utf-8-unix)
  default-sendmail-coding-system 'utf-8-unix
  default-terminal-coding-system 'utf-8-unix

  ; Add newline at bottom of file
  require-final-newline t

  frame-title-format "Emacs"

  ; Backup setups
  backup-directory-alist `((".*" . ,temporary-file-directory))
  auto-save-file-name-transforms `((".*" ,temporary-file-directory t))
  backup-by-copying t
  delete-old-versions t
  kept-new-versions 6
  kept-old-versions 2
  version-control t

  ; Disable auto save
  auto-save-default nil
  ; Disable lockfiles
  create-lockfiles nil

  ; Skip prompt for xref find definition
  xref-prompt-for-identifier nil

  ; Don't wait for keystrokes display
  echo-keystrokes 0.01

  ; Disable margin for overline and underline
  overline-margin 0
  underline-minimum-offset 0

  ; Allow resizing frame by pixels
  frame-resize-pixelwise t

  ; Better scroll behavior
  mouse-wheel-scroll-amount '(1 ((shift) . 1) ((control) . nil))
  mouse-wheel-progressive-speed nil

  ; Disable copy region blink
  copy-region-blink-delay 0

  ; Use short answer when asking yes or no
  read-answer-short t

  ; Always follow link
  vc-follow-symlinks t

  ; Use custom.el as custom file
  custom-file (expand-file-name "custom.el" user-emacs-directory)

  ; Disable ring bell
  visible-bell nil
  ring-bell-function 'ignore

  ; Mouse yank at current point
  mouse-yank-at-point t

  ; DWIM target for dired
  ; Automatically use another dired buffer as target for copy/rename
  dired-dwim-target t

  ; Use spaces instead of tab
  indent-tabs-mode nil

  help-window-select t

  ; Don't echo multiline eldoc
  eldoc-echo-area-use-multiline-p nil

  ; XML indent 4
  nxml-child-indent 4
  nxml-attribute-indent 4

  ; mark limit
  mark-ring-max 6

  isearch-repeat-on-direction-change t
  isearch-wrap-pause 'no-ding
  isearch-lazy-count t

  ; corfu-auto
  corfu-auto-delay 0.1
  corfu-auto-prefix 1

  ; consult complete
  completion-in-region-function 'consult-completion-in-region)

;; fonts
(set-face-attribute 'default nil :family "FantasqueSansM Nerd Font" :weight 'normal :width 'normal :height 110)
(set-face-attribute 'variable-pitch nil :family "Liberation Sans" :weight 'normal :width 'normal)


;; themes
(defvar zmacs-themes '(modus-operandi modus-vivendi))

(defun zmacs-load-theme ()
  (when-let ((theme (car zmacs-themes)))
    (message "Load theme: %s" (car zmacs-themes))
    (mapc 'disable-theme custom-enabled-themes)
    (unless (eq theme 'default)
      (load-theme theme t))))

(defun zmacs-next-theme ()
  (interactive)
  (when zmacs-themes
    (setq zmacs-themes (append (cdr zmacs-themes) (list (car zmacs-themes))))
    (zmacs-load-theme)))

(zmacs-load-theme)


;; org
(setq org-directory "~/Documents/org")
(setq org-agenda-files (list "inbox.org.gpg"))
(defun zmacs-org-capture-inbox ()
     (interactive)
     (call-interactively 'org-store-link)
     (org-capture nil "i"))
(defun zmacs-org-capture-mail ()
  (interactive)
  (call-interactively 'org-store-link)
  (org-capture nil "m"))
(setq org-capture-templates
      `(("i" "Inbox" entry  (file "inbox.org.gpg")
        ,(concat "* TODO %?\n"
                 "/Entered on/ %U"))
        ("m" "Inbox [mu4e]" entry (file "inbox.org.gpg")
        ,(concat "* TODO Process \"%a\" %?\n"
                 "/Entered on/ %U"))))


;; magit
(straight-use-package 'magit)


;; diminish
(straight-use-package 'diminish)
(require 'diminish)
(diminish 'gcmh-mode)
(diminish 'buffer-face-mode)
(diminish 'eldoc-mode)
(diminish 'subword-mode)


;; mood-line
(straight-use-package 'mood-line)
(mood-line-mode)


;; meow
(defun meow-setup ()
  (when window-system
    (setq meow-replace-state-name-list
          '((normal . "🅝")
            (beacon . "🅑")
            (insert . "🅘")
            (motion . "🅜")
            (keypad . "🅚"))))

  (meow-thing-register 'angle
                       '(pair ("<") (">"))
                       '(pair ("<") (">")))
  (meow-thing-register 'angle-inner
                       '(pair (">") ("<"))
                       '(pair (">") ("<")))
  (add-to-list
   'meow-char-thing-table
   '(?a . angle))
  (add-to-list
   'meow-char-thing-table
   '(?i . angle-inner))
  ; hide lighters
  (diminish 'meow-normal-mode)
  (diminish 'meow-motion-mode)
  (diminish 'meow-insert-mode)
  (diminish 'meow-keypad-mode)
  (diminish 'meow-beacon-mode)

  (setq meow-esc-delay 0.001)
  (setq meow-keypad-leader-dispatch "C-c")
  (setq meow--kbd-delete-char "<deletechar>")
  (setq meow--kbd-backward-line "<up>")
  (setq meow--kbd-forward-line "<down>")
  (setq meow--kbd-backward-char "<left>")
  (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
  (setq meow-keypad-meta-prefix nil)
  (setq meow-keypad-ctrl-prefix nil)
  (setq meow-keypad-literal-prefix nil)
  (setq meow-keypad-ctrl-meta-prefix nil)
  (delete 'org-mode meow-expand-exclude-mode-list)
  (apply #'meow-normal-define-key zmacs-meow-normal-keybindings)
  (apply #'meow-motion-overwrite-define-key zmacs-meow-motion-keybindings))


(straight-use-package 'meow)
(require 'meow)
(meow-setup)
(meow-global-mode 1)

;; leader bindings
(defun set-keybind (keybind-info)
  (let* ((keybind (car keybind-info))
         (command (nth 1 keybind-info))
	 (mode-name (nth 2 keybind-info))
	 (mode-hook (intern (format "%s-mode-hook" mode-name)))
	 (mode-map (intern (format "%s-mode-map" mode-name)))
	 (is-global (equal "" mode-name)))
    (if is-global
        (global-set-key (kbd keybind) command)
      (add-hook mode-hook
        (lambda ()
          (define-key (symbol-value mode-map) (kbd (format "%s" keybind)) command))))))
(mapcar #'set-keybind zmacs-meow-leader-keybindings)

;; vertico
(straight-use-package 'vertico)
(vertico-mode)
; Add prompt indicator to `completing-read-multiple'.
; We display [CRM<separator>], e.g., [CRM,] if the separator is a comma.
(defun crm-indicator (args)

  (cons (format "[CRM%s] %s"
                (replace-regexp-in-string
                 "\\`\\[.*?]\\*\\|\\[.*?]\\*\\'" ""
                 crm-separator)
                (car args))
        (cdr args)))
(advice-add #'completing-read-multiple :filter-args #'crm-indicator)

; Do not allow the cursor in the minibuffer prompt
(setq minibuffer-prompt-properties
      '(read-only t cursor-intangible t face minibuffer-prompt))
(add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

; Emacs 28: Hide commands in M-x which do not work in the current mode.
; vertico commands are hidden in normal buffers.
(setq read-extended-command-predicate
      #'command-completion-default-include-p)

; Enable recursive minibuffers
(setq enable-recursive-minibuffers t)

(define-key vertico-map (kbd "C-j") 'vertico-next)
(define-key vertico-map (kbd "C-k") 'vertico-previous)
(define-key vertico-map (kbd "C-l") 'vertico-insert)
(define-key vertico-map (kbd "C-h") 'up-directory)
(define-key vertico-map (kbd "C-p") 'previous-history-element)
(define-key vertico-map (kbd "C-n") 'next-history-element)

;; consult
(straight-use-package 'consult)
;(setq consult-find-args "find .")


;; orderless
(straight-use-package 'orderless)
(setq completion-styles '(orderless basic)
      completion-category-overrides '((file (styles basic partial-completion))))


;; marginalia
(straight-use-package 'marginalia)
(marginalia-mode)


;; corfu
(straight-use-package 'corfu)
(setq
 completion-cycle-threshold 3
 tab-always-indent t
 corfu-cycle t
 corfu-auto t
 corfu-seperator ?\s
 corfu-quit-no-match 'separator)
 (add-hook 'prog-mode-hook 'corfu-mode)
(require 'corfu)

(define-key corfu-map (kbd "C-j") 'corfu-next)
(define-key corfu-map (kbd "C-k") 'corfu-previous)
(define-key corfu-map (kbd "C-l") 'corfu-insert)

(add-hook 'meow-normal-mode-hook 'corfu-quit)

(defun corfu-enable-always-in-minibuffer ()
  "Enable Corfu in the minibuffer if Vertico/Mct are not active."
  (unless (or (bound-and-true-p vertico--input)
              (eq (current-local-map) read-passwd-map))
    ;; (setq-local corfu-auto nil) ;; Enable/disable auto completion
    (setq-local corfu-echo-delay nil ;; Disable automatic echo and popup
                corfu-popupinfo-delay nil)
    (corfu-mode 1)))
(add-hook 'minibuffer-setup-hook #'corfu-enable-always-in-minibuffer 1)


;; embark
;; (straight-use-package 'embark)
;; (add-hook 'eldoc-documentation-functions #'embark-eldoc-first-target)
;; (setq eldoc-documentation-strategy #'eldoc-documentation-compose-eagerly)
;; (add-to-list 'display-buffer-alist
;;                '("\\`\\*Embark Collect \\(Live\\|Completions\\)\\*"
;;                  nil
;;                  (window-parameters (mode-line-format . none))))

;; (straight-use-package 'embark-consult)
;; (add-hook 'embark-collect-mode 'consult-preview-at-point-mode)

;; flycheck
(straight-use-package 'flycheck)
(setq
 flycheck-python-flake8-executable "flake8"
 flycheck-flake8rc "~/.config/flake8"
 flycheck-enabled-checkers '(python-flake8)
 flycheck-disabled-checkers '(python-pylint))


;; avy
(straight-use-package 'avy)


;; lsp-mode
(straight-use-package 'lsp-mode)
(setq lsp-keymap-prefix nil)
(require 'lsp)
(setq lsp-completion-provider :none)
(straight-use-package 'lsp-pyright)
(add-hook 'python-mode-hook (lambda ()
                              (require 'lsp-pyright)
                              (setq
                               lsp-diagnostic-package :none
			       lsp-diagnostics-provider :none)
                              (flycheck-mode)
                              (flycheck-select-checker 'python-flake8)
                              (lsp)))

(straight-use-package 'lsp-ui)


;; virtualenvwrapper
(straight-use-package 'virtualenvwrapper)
(setq venv-location "/home/zaha/.local/share/venvs")
(advice-add 'venv-workon :after (lambda ()
                                  (lsp-restart-workspace)))


;; blacken
(straight-use-package 'blacken)


;; isearch-mb
(straight-use-package 'isearch-mb)
(isearch-mb-mode)


;; treemacs
(straight-use-package 'treemacs)
(setq
 treemacs-expand-after-init nil
 treemacs-expand-added-projects nil)


;; mu4e
(require 'mu4e)
(setq mu4e-drafts-folder "/[Gmail].Drafts")
(setq mu4e-sent-folder   "/[Gmail].Sent Mail")
(setq mu4e-trash-folder  "/[Gmail].Trash")

(setq mu4e-sent-messages-behavior 'delete)

(setq mu4e-maildir-shortcuts
    '( (:maildir "/INBOX"              :key ?i)
       (:maildir "/[Gmail].Sent Mail"  :key ?s)
       (:maildir "/[Gmail].Drafts"  :key ?d)
       (:maildir "/[Gmail].Trash"      :key ?t)))

(add-to-list 'mu4e-bookmarks
   '(:query "maildir:/inbox" :name "Inbox" :key ?i :favorite t))

(setq mu4e-get-mail-command "offlineimap")

(setq message-send-mail-function 'message-send-mail-with-sendmail)
(setq sendmail-program "/usr/bin/msmtp")
(defun choose-msmtp-account ()
  (if (message-mail-p)
      (save-excursion
        (let*
            ((from (save-restriction
                     (message-narrow-to-headers)
                     (message-fetch-field "from")))
             (account
              (cond
               ;; Select an msmtp account based on the value of the email's "From" header.
               ((string-match "zaha@odoo.com" from) "zaha@odoo.com")
               ((string-match "another.address@example.com" from) "another-address"))))
          (setq message-sendmail-extra-arguments (list '"-a" account))))))
(setq message-sendmail-envelope-from 'header)
(add-hook 'message-send-mail-hook 'choose-msmtp-account)

(setq
   user-mail-address "zaha@odoo.com"
   user-full-name  "Zachary Hanham"
   mu4e-compose-signature
    (concat
      "-Zachary Hanham\n"))

;; (require 'smtpmail)
;; (setq
;;  ;message-send-mail-function 'smtpmail-send-it
;;    starttls-use-gnutls t
;;    ;smtpmail-starttls-credentials '(("smtp.gmail.com" 587 nil nil))
;;    smtpmail-default-smtp-server "smtp.gmail.com"
;;    smtpmail-smtp-server "smtp.gmail.com"
;;    smtpmail-smtp-service 587)

(setq message-kill-buffer-on-exit t)

;; (setq smtpmail-auth-credentials
;;       `("smtp.gmail.com"
;;         587
;;         "zaha@odoo.com"
;;         ,(secrets-get-secret "Login" "Password for 'zaha@odoo.com' on 'email_passwords'")))


;; dap
(straight-use-package 'dap-mode)
(setq dap-auto-configure-features '(sessions locals controls tooltip))
(setq
 dap-ui-locals-expand-depth nil)
(require 'dap-python)
;; if you installed debugpy, you need to set this
;; https://github.com/emacs-lsp/dap-mode/issues/306
(setq dap-python-debugger 'debugpy)
(dap-register-debug-template "oe-support"
  (list :type "python"
        :port 5678
        :request "attach"
        :name "oe-support"))
; Workaround for infinite exanding locals buffer
;; (advice-add 'dap-ui-locals--refresh :before
;;             (lambda ()
;;               (let ((window (get-buffer-window)))
;;                 (dap-ui-locals)
;;                 (select-window window))))


;; bookmark-plus
(straight-use-package 'bookmark-plus)


;; elcord
(straight-use-package 'elcord)
(defun get-elcord-mode-text () "🦆")
(setq elcord-display-buffer-details nil
      elcord-show-small-icon nil
      elcord-display-elapsed nil)
(advice-add 'elcord--mode-text :override #'get-elcord-mode-text)
(elcord-mode)


;; emms
(straight-use-package 'emms)
(emms-all)
(require 'emms-player-mpd)
(setq emms-player-list '(emms-player-mpd)
      emms-info-functions '(emms-info-mpd)
      emms-player-mpd-music-directory "~/Music"
      emms-mode-line-icon-enabled-p nil)
(emms-history-load)
;; covers
(setq emms-browser-covers #'emms-browser-cache-thumbnail-async)
(setq emms-browser-thumbnail-small-size 64)
(setq emms-browser-thumbnail-medium-size 128)
;; system-notify: make a nice notification widget on the screen
(defun system-notify (title message)
  "invoke easy-notify with a title and shows a message"
  (interactive "")
  (start-process-shell-command "*Output*" nil
			       (replace-regexp-in-string "&" "&amp;" (concat "easy-notify -t \"" title "\" -m \"" message "\""))))

;; use system-notify with emms
(add-hook
 'emms-player-started-hook
 '(lambda ()(system-notify
	     (emms-track-description
              (emms-playlist-current-selected-track))
	     "")
    ))
