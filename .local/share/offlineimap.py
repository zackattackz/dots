from keyring import get_password

def password_for(email):
    return get_password("email_passwords", email)
