source ~/.config/nvim/plug.vim

set termguicolors
set background=light

colorscheme modus-operandi

nnoremap <SPACE> <Nop>
map <SPACE> <Leader>
set timeoutlen=900

set relativenumber

let no_plugin_maps = 1 
let g:dispatch_no_maps = 1

map H ^
map L $

filetype plugin on
set omnifunc=syntaxcomplete#Complete

let g:airline_powerline_fonts = 1
let g:airline_theme='solarized'
let g:airline_solarized_bg='light'

let g:NERDCreateDefaultMappings = 0
let g:NERDSpaceDelims = 1
let g:NERDCompactSexyComs = 1
let g:NERDCommentEmptyLines = 1
let g:NERDTreeQuitOnOpen = 1

let g:user_emmet_install_global = 0
let g:user_emmet_leader_key='<C-Z>'
autocmd FileType html,css EmmetInstall


" easymotion binds
map  <Leader>f <Plug>(easymotion-bd-f2)
nmap <Leader>f <Plug>(easymotion-overwin-f2)
map <Leader>G <Plug>(easymotion-bd-jk)
nmap <Leader>G <Plug>(easymotion-overwin-line)

" bindings for tab management
nnoremap <C-w>c :tabnew<CR>
nnoremap <C-w>n :tabn<CR>
nnoremap <C-w>p :tabp<CR>

" NERDTree open/close -> C-t
nnoremap <C-t> :call <SID>NERDTreeCustomFocus()<CR>

" returns 1 if current window is NERDTree, false otherwise
function! s:IsCurrentWindowNERDTree()
  return exists("t:NERDTreeBufName") && bufwinnr(t:NERDTreeBufName) == winnr()
endfunction

" Closes tree if focused, else refreshes and focuses
function! s:NERDTreeCustomFocus()
  if s:IsCurrentWindowNERDTree()
    NERDTreeClose
  else
    NERDTreeRefreshRoot
    NERDTreeFocus
  endif
endfunction

" autoclose NERDTree if it is last buffer
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" <C-q> is <C-w>q
map <C-q> <C-w>q

" focus current buffer
nnoremap <Leader>q :only<CR>

" easier pane navigation
map <C-h> <C-w>h
map <C-l> <C-w>l
map <C-k> <C-w>k
map <C-j> <C-w>j

" <C-n> to toggle line nums (for copying txt)
map <C-n> :set invrelativenumber<CR>

" fzf Files binding
nnoremap <Leader><SPACE> :Files<CR>
nnoremap <Leader><Tab> :GFiles<CR>

" git mappings
" G
nnoremap <Leader>gg :G<CR>
" GWrite
nnoremap <Leader>gw :Gwrite<CR>
" git commit
nnoremap <Leader>gc :G commit<CR>
" git diff
nnoremap <Leader>gdd :G diff<CR>
" git diffsplit
nnoremap <Leader>gds :Gdiffsplit<CR>
" git log
nnoremap <Leader>gl :G log<CR>

" dispatch binds
" Make
nnoremap <Leader>mm :Make<CR>
" Make!
nnoremap <Leader>md :Make!<CR>
" Copen
nnoremap <Leader>mo :Copen<CR>

" code manipulation mappings
nnoremap <Leader>cc <plug>NERDCommenterToggle<CR>
vnoremap <Leader>cc <plug>NERDCommenterToggle<CR>

" Clear Highlights
nnoremap <Leader>/ :noh<CR>
